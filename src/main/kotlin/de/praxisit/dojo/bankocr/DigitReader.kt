package de.praxisit.dojo.bankocr

import de.praxisit.dojo.bankocr.Digits.digitWidth
import de.praxisit.dojo.bankocr.Digits.digitHeight

/**
 * Read the content of the input as a block digit with [digitHeight] and [digitWidth].
 *
 * @property lines alle lines with digit parts
 * @property position current position of the reader
 *
 * @author Bernd Kursawe (bernd.kursawe@praxisit.de)
 * @since 07.04.18
 */
class DigitReader(private val lines: List<String>,
    private var position: Int = 0) : Iterable<String>, Iterator<String> {

    /**
     * Secondary constructor which splits the input string in the different lines.
     * @param input an input string
     */
    constructor(input: String) : this(input.split("\n").take(digitHeight))

    override fun iterator(): Iterator<String> = this

    override fun hasNext(): Boolean = isCorrectPosition(position)

    override fun next(): String = readDigit(position++)

    /**
     * Read a single digit, starting from a [position].
     * @param position position in the string starting from 0
     * @return a block digit which contains all parts of the digit representation as a
     * single string (without delimiter)
     */
    private fun readDigit(position: Int): String {
        if (position >= count()) return ""

        val index = position * digitWidth
        return lines.fold("") { old, line -> old + line.substring(index, index + digitWidth) }
    }

    /**
     * Check if the [position] is in the input string.
     * @param position position under test
     * @return `true` if the position is in the input, otherwise `false`
     */
    private fun isCorrectPosition(position: Int) = position < count()

    /**
     * Count the number of block digit representations in the input.
     * If the lines have different length, the shortest length is used
     * @return minimal number of digits in the input string
     */
    fun count() = lines.fold(Int.MAX_VALUE) { old, next -> Math.min(old, next.length) } /
        digitWidth

}
