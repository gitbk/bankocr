package de.praxisit.dojo.bankocr

/**
 * Digit representations.
 *
 * @author Bernd Kursawe (bernd.kursawe@praxisit.de)
 * @since 07.04.18
 */
object Digits {
    /** Width of a digit. */
    const val digitWidth = 3

    /** Height of a digit. */
    const val digitHeight = 4

    // @formatter:off
    /** Representation of 0. */
    private const val digit0 =
            " _ " +
            "| |" +
            "|_|" +
            "   "
    /** Representation of 1. */
    private const val digit1 =
            "   " +
            "  |" +
            "  |" +
            "   "
    /** Representation of 2. */
    private const val digit2 =
            " _ " +
            " _|" +
            "|_ " +
            "   "
    /** Representation of 3. */
    private const val digit3 =
            " _ " +
            " _|" +
            " _|" +
            "   "
    /** Representation of 4. */
    private const val digit4 =
            "   " +
            "|_|" +
            "  |" +
            "   "
    /** Representation of 5. */
    private const val digit5 =
            " _ " +
            "|_ " +
            " _|" +
            "   "
    /** Representation of 6. */
    private const val digit6 =
            " _ " +
            "|_ " +
            "|_|" +
            "   "
    /** Representation of 7. */
    private const val digit7 =
            " _ " +
            "  |" +
            "  |" +
            "   "
    /** Representation of 8. */
    private const val digit8 =
            " _ " +
            "|_|" +
            "|_|" +
            "   "
    /** Representation of 9. */
    private const val digit9 =
            " _ " +
            "|_|" +
            " _|" +
            "   "

    private val digitMap = mapOf(
            '0' to digit0,
            '1' to digit1,
            '2' to digit2,
            '3' to digit3,
            '4' to digit4,
            '5' to digit5,
            '6' to digit6,
            '7' to digit7,
            '8' to digit8,
            '9' to digit9)
    // @formatter:on

    /**
     * Search for the representation of a [digit].
     * @param digit digit to find
     * @return string representation
     */
    fun representationFor(digit: Char) = digitMap[digit]

    /**
     * Search for the matching digit of the [str].
     * @param str string representation
     * @return matching digit or `null`
     */
    fun find(str: String) = digitMap.entries.firstOrNull { it.value == str }?.key

}