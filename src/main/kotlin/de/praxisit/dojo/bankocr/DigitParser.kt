package de.praxisit.dojo.bankocr

/**
 * Parser for a string with digit representations.
 *
 * @property reader a [DigitReader] for read the digit representations
 *
 * @author Bernd Kursawe (bernd.kursawe@praxisit.de)
 * @since 07.04.18
 */
class DigitParser(private val reader: DigitReader) : Iterator<Char>, Iterable<Char> {

    /**
     * Secondary constructor that transforms an input string to a [DigitReader].
     * @param input string with digit representations
     */
    constructor(input: String) : this(DigitReader(input))

    override fun iterator(): Iterator<Char> = this

    override fun hasNext() = reader.hasNext()

    override fun next(): Char {
        val text = reader.next()
        return Digits.find(text) ?: '?'
    }

    /**
     * Parse all digits from the input to the end.
     * @return a string with all digits
     */
    fun parseAll() = this.fold("") { old, next -> old + next }

    /**
     * Count the digits in the input.
     * @return number of digits
     */
    fun count() = reader.count()

}