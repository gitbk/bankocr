package de.praxisit.dojo.bankocr

import de.praxisit.dojo.bankocr.Digits.digitWidth
import de.praxisit.dojo.bankocr.Digits.digitHeight

/**
 * Generates LED like digits from digit [Char]s. The output string consists out of some
 * lines where each line ends with an `\n`.
 *
 * @property buffer internal buffer for output string
 *
 * @author Bernd Kursawe (bernd.kursawe@praxisit.de)
 * @since 07.04.18
 */
class DigitGenerator(private val buffer: Array<StringBuilder> = Array(digitHeight) { StringBuilder() }) {

    /**
     * Add a single digit to the generator buffer. If the `digit` is not known, a _blank_
     * character is produced.
     *
     * @param digit a single digit
     * @sample addDigit('0')
     */
    fun addDigit(digit: Char) {
        val digitString = Digits.representationFor(digit) ?: " ".repeat(digitHeight * digitWidth)
        for (i in 0 until digitHeight) {
            val start = i * digitWidth
            val end = start + digitWidth
            if (end <= digitString.length) buffer[i].append(digitString.substring(start, end))
        }
    }

    /**
     * Add some digits to the output buffer.
     *
     * @param digits a string with 0 or more digits
     */
    fun addDigits(digits: String) {
        digits.forEach { addDigit(it) }
    }

    /**
     * Create the output string from the buffer where every line ends with `\n`.
     *
     * @return a string with the digit representations
     */
    fun createString(): String = buffer.joinToString("\n") + "\n"

}
