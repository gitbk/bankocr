package de.praxisit.dojo.bankocr

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class DigitsTest {

    @Test
    fun exists0() {
        val digit = Digits.representationFor('0')
        assertThat(digit).isNotNull()
        assertThat(digit).isEqualTo(" _ | ||_|   ")
    }

    @Test
    fun exists1() {
        val digit = Digits.representationFor('1')
        assertThat(digit).isNotNull()
        assertThat(digit).isEqualTo("     |  |   ")
    }

    @Test
    fun findKnownDigit() {
        val digit = Digits.find("     |  |   ")
        assertThat(digit).isEqualTo('1')
    }

    @Test
    fun findUnknownDigit() {
        val digit = Digits.find("            ")
        assertThat(digit).isNull()
    }


}