package de.praxisit.dojo.bankocr

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

/**
 * @author Bernd Kursawe (bernd.kursawe@praxisit.de)
 * @since 07.04.18
 */
class DigitReaderTest {

    private val emptyDigit = ""
    private val emptyLineDigit = "\n\n\n\n"
    private val incompleteLine = " \n \n \n \n"
    private val differentLineLengths = "\n   \n \n  \n"
    private val blankDigit = "   \n   \n   \n   \n"
    private val digit1 =
        "   \n" +
            "  |\n" +
            "  |\n" +
            "   \n"
    private val digits =
        " _     _  _     _  _  _  _  _ \n" +
        "| |  | _| _||_||_ |_   ||_||_|\n" +
        "|_|  ||_  _|  | _||_|  ||_| _|\n" +
        "                              \n"

    @Test
    fun readHasNextEmptyInput() {
        val reader = DigitReader(emptyDigit)
        val hasNext = reader.hasNext()
        assertThat(hasNext).isFalse()
    }

    @Test
    fun readHasNextEmptyLineInput() {
        val reader = DigitReader(emptyLineDigit)
        val hasNext = reader.hasNext()
        assertThat(hasNext).isFalse()
    }

    @Test
    fun readHasNextIncomplete() {
        val reader = DigitReader(incompleteLine)
        assertThat(reader.hasNext()).isFalse()
    }

    @Test
    fun readHasNextDifferentLineLengths() {
        val reader = DigitReader(differentLineLengths)
        assertThat(reader.hasNext()).isFalse()
    }

    @Test
    fun readHasNextBlankDigit() {
        val reader = DigitReader(blankDigit)
        assertThat(reader.hasNext()).isTrue()
    }

    @Test
    fun readNextBlankDigit() {
        val reader = DigitReader(blankDigit)
        assertThat(reader.next()).isEqualTo("            ")
    }

    @Test
    fun readNextDigit1() {
        val reader = DigitReader(digit1)
        assertThat(reader.next()).isEqualTo("     |  |   ")
    }

    @Test
    fun readCountDigits() {
        val reader = DigitReader(digits)
        assertThat(reader.count()).isEqualTo(10)
    }

}