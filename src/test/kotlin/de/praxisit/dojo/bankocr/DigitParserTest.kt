package de.praxisit.dojo.bankocr

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

/**
 * @author Bernd Kursawe (bernd.kursawe@praxisit.de)
 * @since 07.04.18
 */
internal class DigitParserTest {

    // @formatter:off
    private val digits =
        " _     _  _     _  _  _  _  _ \n" +
        "| |  | _| _||_||_ |_   ||_||_|\n" +
        "|_|  ||_  _|  | _||_|  ||_| _|\n" +
        "                              \n"
    // @formatter:on

    @Test
    fun parseEmptyString() {
        val parser = DigitParser("")
        assertThat(parser.iterator().hasNext()).isFalse()
    }

    @Test
    fun parseFirstDigit() {
        val parser = DigitParser(digits)
        val iterator = parser.iterator()
        assertThat(iterator.hasNext()).isTrue()
        assertThat(iterator.next()).isEqualTo('0')
    }

    @Test
    fun parseDigitCount() {
        val parser = DigitParser(digits)
        assertThat(parser.count()).isEqualTo(10)
    }

    @Test
    fun parseInDigits() {
        val parser = DigitParser(digits)
        var count = 0
        for (element in parser) {
            count++
        }
        assertThat(count).isEqualTo(10)
    }

    @Test
    fun parseAllDigits() {
        val parser = DigitParser(digits)
        assertThat(parser.count()).isEqualTo(10)
        val result = parser.parseAll()
        assertThat(result).isEqualTo("0123456789")
    }

    @Test
    fun parseGeneratedDigits() {
        val generator = DigitGenerator()
        val digits = "7897651232468"
        generator.addDigits(digits)
        val digitString = generator.createString()
        val parser = DigitParser(digitString)
        assertThat(parser.parseAll()).isEqualTo(digits)
    }


}